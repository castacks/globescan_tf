#include <ros/ros.h>
#include <math.h>
#include <ros/console.h>
#include <std_msgs/String.h>
#include <tf/transform_broadcaster.h>
#include "TimeoutSerial.h"
#include <boost/cstdint.hpp>
#include <iostream>
#include <sstream>
#include <string>

// Defined as pitch from horizontal. Note in laser coordinates an upward pitch is negative.
//#define LASER_PITCH -M_PI/4
#define ENCODER_OFFSET 5000
#define ENCODER__RANGE 2048

struct encoderData {
  float yaw_rad;
  int elapseSincePPS_us;
  int rawAngle, rawVelocity;
};



void encoderStringCallback( std_msgs::String rawEncoderString) {

  static  tf::TransformBroadcaster br;
  tf::Transform encoderTransform;

  ros::Time playbackTime = ros::Time::now();
  
  std::istringstream input(rawEncoderString.data);
  
  encoderData data;
  
  // Read the ASCII values in the string into the variables. This relies on the fact that we know the structure of the incoming data.
  input >> data.rawAngle >> data.rawVelocity >> data.elapseSincePPS_us;

  data.yaw_rad = -1 * (float)(data.rawAngle - ENCODER_OFFSET) * 2*M_PI / ENCODER__RANGE; 

  
  encoderTransform.setOrigin(tf::Vector3(0, 0, 0) ); // Simply assumed to be at the center of the globescan
  encoderTransform.setRotation( tf::createQuaternionFromRPY(-data.yaw_rad - M_PI/2,  0, 0));
  
  br.sendTransform(tf::StampedTransform(encoderTransform, playbackTime, "base_frame", "motor_shaft"));

  std::cout << data.yaw_rad << std::endl; 
 
}


int main(int argc, char** argv) {

  ros::init(argc, argv, "globescan_encoder_string_to_tf_broadcaster");
  ros::NodeHandle n;

  ros::Subscriber sub_imu = n.subscribe("encoderRawData", 1, encoderStringCallback);

  ros::spin();

  return 0;

}
