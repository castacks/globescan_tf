#include "ros/ros.h"
#include "std_msgs/String.h"
#include <iostream>
#include <vector>
#include <tf/transform_broadcaster.h>
#include "sensor_msgs/Imu.h"
#include <math.h>
#include <nav_msgs/Odometry.h>


tf::Vector3 positionVector(0,0,0);
// Defined as pitch from horizontal.
double laser_pitch_deg = 0;
double laser_twist_deg = 0.5;
double emitterTranslation_m;
//float laser_pitch_rad = 45 *( -M_PI/180);

void odomCallback(const nav_msgs::Odometry::ConstPtr& odomMsg)
{
  static tf::TransformBroadcaster br;
  static tf::Transform odomTF;

	odomTF.setOrigin(tf::Vector3(	odomMsg->pose.pose.position.x, 
					odomMsg->pose.pose.position.y, 
					odomMsg->pose.pose.position.z) ); 

	odomTF.setRotation( tf::Quaternion(	odomMsg->pose.pose.orientation.x,
						odomMsg->pose.pose.orientation.y,
						odomMsg->pose.pose.orientation.z,
						odomMsg->pose.pose.orientation.w)); 

	br.sendTransform(tf::StampedTransform(	odomTF, 
						odomMsg->header.stamp, 
						odomMsg->header.frame_id, 
						odomMsg->child_frame_id));


  tf::Transform laserTilt;
  tf::Transform laserDetectorGeometry;

ros::param::getCached("/laser_pitch_deg", laser_pitch_deg);
      
      double laser_twist_rad = laser_twist_deg * M_PI/180;
      double laser_pitch_rad = laser_pitch_deg * M_PI/180;


      // Note: All quaternions defined with only 3 elements are defined in Euler angles and converted upon creation.
      
      laserTilt.setOrigin(tf::Vector3(0, 0, -0.160) ); // Roughly measured from center of mass of Globescan
      laserTilt.setRotation( tf::createQuaternionFromRPY(0,  M_PI + laser_pitch_rad, 0)); // Compensate for twisting around the scan axis % 0.35

      //if (!node_handle_private.getParamCached("emitterTranslation_m", emitterTranslation_m))
      //emitterTranslation = 0.035;    
      ros::param::getCached("/emitterTranslation_m", emitterTranslation_m);  

      laserDetectorGeometry.setOrigin(tf::Vector3(0, 0, emitterTranslation_m) );
      laserDetectorGeometry.setRotation( tf::createQuaternionFromRPY(laser_twist_rad, 0, 0));
      
      br.sendTransform(tf::StampedTransform(laserTilt, odomMsg->header.stamp, "motor_shaft", "laser_base"));
      br.sendTransform(tf::StampedTransform(laserDetectorGeometry, odomMsg->header.stamp, "laser_base", "laser"));

}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "static_tf_broadcaster");
  ros::NodeHandle n;
  ros::NodeHandle node_handle_private = ros::NodeHandle("~");
  ros::Subscriber sub_odom = n.subscribe("/encoderOdom", 100, odomCallback);
  //ros::param::get("/laser_pitch_deg", laser_pitch_deg);
  ros::param::get("/laser_twist_deg", laser_twist_deg);

  ros::spin();

  return 0;
}
