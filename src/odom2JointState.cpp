/*
 * odom2JointState.cpp
 *
 *  Created on: Mar 12, 2013
 *      Author: achamber
 */

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/JointState.h>
#include <ca_common/math.h>

using namespace CA;

ros::Publisher jointstate_pub;


void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{

	sensor_msgs::JointState js;

	js.header = msg->header;
	js.name.push_back("motor_shaft_spin");

	QuatD q = msgc(msg->pose.pose.orientation);
	Vector3D rot = quatToEuler(q);

	js.position.push_back(rot[2]);
	js.velocity.push_back(msg->twist.twist.angular.z);

	jointstate_pub.publish(js);


}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "odom2JointState_publisher");
	ros::NodeHandle n;
	ros::NodeHandle np("~");
	ros::Subscriber sub = n.subscribe("odom", 100, odomCallback);
	jointstate_pub = n.advertise<sensor_msgs::JointState>("joint_states", 50);

	ros::spin();


	return 0;
}


