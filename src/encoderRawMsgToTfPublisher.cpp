#include <ros/ros.h>
#include <math.h>
#include <ros/console.h>
#include <std_msgs/String.h>
#include <tf/transform_broadcaster.h>
#include <boost/cstdint.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <nav_msgs/Odometry.h>

// Defined as pitch from horizontal. Note in laser coordinates an upward pitch is negative.

double encoder_offset; 
bool have_checksum;
ros::Publisher odom_pub;

struct encoderData {
  double yaw_rad,yawrate;
  int elapseSincePPS_us;
  double rawAngle, rawVelocity;
};



void encoderStringCallback( std_msgs::String rawEncoderString) {

  static  tf::TransformBroadcaster br;
  tf::Transform encoderTransform;

  ros::Time playbackTime = ros::Time::now();
  
  std::istringstream input(rawEncoderString.data);
  
  encoderData data;
  unsigned char index, checksum;

  // Read the ASCII values in the string into the variables. This relies on the fact that we know the structure of the incoming data.
  if(have_checksum)
    input >> data.rawAngle >> data.rawVelocity >> data.elapseSincePPS_us 
	  >> std::hex >> index >> std::hex >> checksum;
  else
    input >> data.rawAngle >> data.rawVelocity >> data.elapseSincePPS_us;

  data.yaw_rad = (data.rawAngle - encoder_offset) *M_PI /180.0; 
  data.yawrate = data.rawVelocity*M_PI /180.0; 
  
  //std::cout <<"raw " <<data.rawAngle<<" offset "<<encoder_offset<<std::endl;
  
  encoderTransform.setOrigin(tf::Vector3(0, 0, 0) ); // Simply assumed to be at the center of the globescan
  encoderTransform.setRotation( tf::Quaternion(data.yaw_rad,  0, 0));
  
  br.sendTransform(tf::StampedTransform(encoderTransform, playbackTime, "motor_base", "motor_shaft"));

// Create and publish the odometry message over ROS
  nav_msgs::Odometry odom;
  odom.header.stamp = playbackTime;
  odom.header.frame_id = "motor_base";
  
  geometry_msgs::Quaternion encoder_quat = tf::createQuaternionMsgFromYaw(data.yaw_rad );
  
  // Set the position
  odom.pose.pose.position.x = 0;
  odom.pose.pose.position.y = 0;
  odom.pose.pose.position.z = 0;   
  odom.pose.pose.orientation =  encoder_quat;
  
  // Set the velocity
  odom.child_frame_id = "motor_shaft";
  odom.twist.twist.angular.z = data.yawrate;
  // Publish odometry message
  odom_pub.publish(odom);
}


int main(int argc, char** argv) {

  ros::init(argc, argv, "encoder_string_to_tf_broadcaster");
  ros::NodeHandle n;
  ros::NodeHandle node_handle_private = ros::NodeHandle("~");
  node_handle_private.getParam("encoder_offset", encoder_offset);
  std::cout<<"offset set to "<<encoder_offset<<std::endl;
  node_handle_private.param<bool>("use_checksum", have_checksum,true);
  std::cout<<"have_checksum set to "<<have_checksum<<std::endl;
  encoder_offset=encoder_offset;
  odom_pub = n.advertise<nav_msgs::Odometry>("encoderOdom", 50);
  ros::Subscriber sub_imu = n.subscribe("encoderRawData", 1, encoderStringCallback);

  ros::spin();

  return 0;

}
