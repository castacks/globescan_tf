#include <math.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <ros/ros.h>
#include <ros/console.h>
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>
#include <boost/cstdint.hpp>
#include "TimeoutSerial.h"
#include <ca_sensor_msgs/StringStamped.h>

// Laser Pitch: Defined as pitch from horizontal. Note in laser coordinates an upward pitch is negative.
#define MIN_TIME_DIFF 0.01
double velocity_conversion_factor = -0.001234; // 1.0 for UTRC
double max_speed = 999999;   // ~400 for UTRC
double encoder_range = 2048; // 360.0 for UTRC
double encoder_offset = 5000; // ?? calibrate for UTRC
int encoder_command_width = 5;  // 3 for UTRC

struct encoderData {
  double yaw_rad, velocity_rad_per_sec;
  double elapseSincePPS_s;
  double rawAngle, rawVelocity;
  unsigned int index, checksum;
};

encoderData readEncoder(TimeoutSerial *serialHandle);
int setMotorSpeed(TimeoutSerial *serialHandle, unsigned int speed);
int enableMotor(TimeoutSerial *serialHandle);
int disableMotor(TimeoutSerial *serialHandle);
ros::Time fixTimestamps(ros::Time cpu, int elapseTime_ns);

/**
 readEncoder - parses the serial stream containing encoder readings from the microcontroler
 
 returns: encoderData structure - defined above

*/
encoderData readEncoder(TimeoutSerial *serialHandle) {

  std::string line = serialHandle->readStringUntil();
  std::istringstream input(line);
  encoderData data;

  // Read the ASCII values in the string into the variables. This relies on the fact that we know the structure of the incoming data.
  input >> data.rawAngle >> data.rawVelocity >> data.elapseSincePPS_s 
	>> std::hex >> data.index >> std::hex >> data.checksum;
	std::cout << data.rawAngle <<" " <<data.rawVelocity <<" "<<data.elapseSincePPS_s <<" "
       << std::hex << data.index <<" "<< std::hex << data.checksum<<std::endl;
  
  // Convert the raw units of rawAngle into radians 
  // Switch sign to use right-handed coordinates
  data.yaw_rad = (float)(data.rawAngle - encoder_offset) * M_PI / 180.0; 
  
  data.velocity_rad_per_sec = (float)data.rawVelocity * M_PI/180.0;

  return data; 
}


int setMotorSpeed(TimeoutSerial *serialHandle, unsigned int speed) {

  if(speed > max_speed)
    return 0;

  std::stringstream speedSS;
  speedSS << std::setfill('0') << std::setw(encoder_command_width) << speed << "g"; 

  std::string s = speedSS.str();
  std::cout << s;

  serialHandle->writeString(s);

  std::string reply = serialHandle->readStringUntil();
  std::cout << "Reply: " << reply << std::endl;
  
  return 1;
}

int enableMotor(TimeoutSerial *serialHandle) {
  for (int i=0;i<10;i++)
    {
      serialHandle->writeString("e");
      usleep(10);
    }
  return 1;
}

int disableMotor(TimeoutSerial *serialHandle) {
  for (int i=0;i<10;i++)
    {
      serialHandle->writeString("d");
      usleep(10);
    }
  return 1;
}

ros::Time fixTimestamps(ros::Time cpu, int elapseTime_ns)
{
  ros::Time fixedTime;
  const int half_second_ns = 0.5e9; 
  
  int diff = cpu.nsec - elapseTime_ns;

  if (diff > half_second_ns)
    {
      fixedTime.sec = cpu.sec + 1;
    }
  else if (diff > (-1* half_second_ns))
    {
      fixedTime.sec = cpu.sec;
    }
  else
    {
      fixedTime.sec = cpu.sec - 1;
    }
  fixedTime.nsec = elapseTime_ns;

  return fixedTime;
}



int main(int argc, char** argv) {

  ros::init(argc, argv, "globescan_encoder_tf_broadcaster");

  ros::NodeHandle node;
  ros::NodeHandle node_handle_private = ros::NodeHandle("~");

  tf::TransformBroadcaster br;
  tf::Transform encoderTransform;

  ros::Publisher odom_pub = node.advertise<nav_msgs::Odometry>("encoderOdom", 50);
  ros::Publisher rawData_pub = node.advertise<ca_sensor_msgs::StringStamped>("encoderRawData", 50);
  ca_sensor_msgs::StringStamped rawString;

  int currentSpeed = 1000;
  int desiredSpeed;

  // read encoder setup parameters (default to globescan values)
  node_handle_private.getParam("velocity_conversion_factor", velocity_conversion_factor);
  node_handle_private.getParam("max_speed", max_speed);
  node_handle_private.getParam("encoder_range", encoder_range);
  node_handle_private.getParam("encoder_offset", encoder_offset);
  node_handle_private.getParam("encoder_command_width", encoder_command_width);

  // Read input settings
  std::string devname;
  if (!node_handle_private.getParam("devname", devname))
    devname = "/dev/ttyUSB0";
  
  int baudrate;
  if (!node_handle_private.getParam("baudrate", baudrate))
    baudrate = 57600;

  bool usePPS;
  //if (!node_handle_private.getParam("use_pps", usePPS))
  //  usePPS = false;  

  // Create serial port reader
  TimeoutSerial serial(devname,baudrate);
  serial.setTimeout(boost::posix_time::seconds(5));
  
  encoderData data; 

  // Initialize previous time to zero
  ros::Time prev(0);

  enableMotor(&serial);

  while (ros::ok()) {

    if (!node_handle_private.getParamCached("use_pps", usePPS))
      usePPS = false;  

    if (!node_handle_private.getParamCached("spinnerSpeed", desiredSpeed))
      desiredSpeed = 100;
 
    if (currentSpeed != desiredSpeed)
      {
	      if(setMotorSpeed(&serial, desiredSpeed))
	      {
	        currentSpeed = desiredSpeed;
        }
      }

    // Reads the encoder data from the serial port
    data = readEncoder(&serial);
    
    // Immediately record the system time
    ros::Time now = ros::Time::now();
		rawString.header.stamp = now;
	
    // If we are using the information in the data stream about offset time from the PPS pulse, modify the time stamp to match
    if (usePPS) 
      {
	      int elapseSincePPS_ns = data.elapseSincePPS_s * 1e9;
	      now = fixTimestamps(now, elapseSincePPS_ns);
      }

    // We check the time difference to filter out times when the microcontroller sends multiple readings twice in a row
    ros::Duration timeDifference = now - prev;
    if (timeDifference.toSec() > MIN_TIME_DIFF)
      {
	      prev = now;

	      encoderTransform.setOrigin(tf::Vector3(0, 0, 0) ); 
	      encoderTransform.setRotation( tf::createQuaternionFromRPY(data.yaw_rad,  0, 0));  
          
	      // Publish the ROS transform for the spin angle
	      br.sendTransform(tf::StampedTransform(encoderTransform, now, "motor_base", "motor_shaft"));

	      // Create and publish the odometry message over ROS
	      nav_msgs::Odometry odom;
	      odom.header.stamp = now;
	      odom.header.frame_id = "motor_base";

	      geometry_msgs::Quaternion encoder_quat = tf::createQuaternionMsgFromYaw(data.yaw_rad);

	      // Set the position
	      odom.pose.pose.position.x = 0;
	      odom.pose.pose.position.y = 0;
	      odom.pose.pose.position.z = 0;   
	      odom.pose.pose.orientation =  encoder_quat;

	      // Set the velocity
	      odom.child_frame_id = "motor_shaft";
	      odom.twist.twist.angular.z = data.velocity_rad_per_sec;

	      // Publish odometry message
	      odom_pub.publish(odom);

	      // Publish the raw string from the serial port
	      std::stringstream ss;
	      ss << data.rawAngle << "," << data.rawVelocity << "," << data.elapseSincePPS_s 
		<<","<<std::hex<<data.index<< ","<<std::hex<<data.checksum<< std::endl;
	      rawString.data = ss.str();
	      rawData_pub.publish(rawString);
    
      } // end filter statement to check for time difference

    ros::spinOnce();
  }
  disableMotor(&serial);
  return 0;
}
